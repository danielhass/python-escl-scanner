from setuptools import setup, find_packages

setup(
    name="escl-scanner",
    version="0.1.0",
    description="A small ESCL scanning library",
    packages=['escl_scanner'],
    python_requires='>=3.6',
    install_requires=[
        "lxml==4.9.4"
    ]
)
