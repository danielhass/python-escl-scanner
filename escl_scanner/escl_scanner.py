#!/usr/bin/python
from fileinput import filename
import urllib.parse, urllib.error, urllib.request, urllib.error, urllib.parse, sys, tempfile, logging
from lxml import etree

class ESCLScanner:

    scanns = "http://schemas.hp.com/imaging/escl/2011/05/03"
    pwgns = "http://www.pwg.org/schemas/2010/12/sm"

    onam = None
    resolution = None
    scanner = None

    timeout = 30
    sb = None

    def __init__(self, ip, port):
        self.scanner_host = ip
        self.scanner_port = port
        self.scanner = f"http://{self.scanner_host}:{self.scanner_port}/eSCL/"

    def scan(self, filepath=None):

        onam = filepath

        etree.register_namespace('scan', self.scanns)
        etree.register_namespace('pwg', self.pwgns)

        req = urllib.request.Request(url = self.scanner+'ScannerCapabilities')
        tree = etree.parse(urllib.request.urlopen(req))
        print("Scanner information:")
        print(etree.tostring(tree, pretty_print=True))

        maxwid = etree.ETXPath("//{%s}MaxWidth" % self.scanns)(tree)[0].text
        maxhei = etree.ETXPath("//{%s}MaxHeight" % self.scanns)(tree)[0].text

        if not self.resolution:
            maxxr = etree.ETXPath("//{%s}MaxOpticalXResolution" % self.scanns)(tree)[0].text
            maxyr = etree.ETXPath("//{%s}MaxOpticalYResolution" % self.scanns)(tree)[0].text
            self.resolution = min(int(maxxr), int(maxyr))

        req = etree.Element("{%s}ScanSettings" % self.scanns, nsmap={"pwg":self.pwgns})
        etree.SubElement(req, "{%s}Version" % self.pwgns).text = "2.6"
        srs = etree.SubElement(req, "{%s}ScanRegions" % self.pwgns)
        sr = etree.SubElement(srs, "{%s}ScanRegion" % self.pwgns)
        etree.SubElement(sr, "{%s}XOffset" % self.pwgns).text = "0"
        etree.SubElement(sr, "{%s}YOffset" % self.pwgns).text = "0"
        etree.SubElement(sr, "{%s}Width" % self.pwgns).text = maxwid
        etree.SubElement(sr, "{%s}Height" % self.pwgns).text = maxhei
        etree.SubElement(sr, "{%s}ContentRegionUnits" % self.pwgns).text = "escl:ThreeHundredthsOfInches"
        etree.SubElement(req, "{%s}InputSource" % self.scanns).text = "Platen"
        # Default is usually RGB24 anyway
        etree.SubElement(req, "{%s}ColorMode" % self.scanns).text = "RGB24"
        etree.SubElement(req, "{%s}XResolution" % self.scanns).text = str(self.resolution)
        etree.SubElement(req, "{%s}YResolution" % self.scanns).text = str(self.resolution)
        etree.SubElement(req, "{%s}Brightness" % self.scanns).text = "1000"
        etree.SubElement(req, "{%s}Contrast" % self.scanns).text = "1000"

        print("Our scan request:")
        print(etree.tostring(req, pretty_print=True, xml_declaration=True, encoding="utf-8"))

        # Post the request:
        xml = etree.tostring(req, xml_declaration=True, encoding="UTF-8")
        req = urllib.request.Request(url = self.scanner+'ScanJobs', data=xml, headers={'Content-Type': 'text/xml'})
        location = None
        try:

            hh = urllib.request.HTTPHandler()
            hsh = urllib.request.HTTPSHandler()
            hh.set_http_debuglevel(1)
            hsh.set_http_debuglevel(1)
            opener = urllib.request.build_opener(hh, hsh)
            logger = logging.getLogger()
            logger.addHandler(logging.StreamHandler(sys.stdout))
            logger.setLevel(logging.NOTSET)
            # opener.open(req)
            response = urllib.request.urlopen(req)
            print(response.getheader('Location'))
            location = response.getheader('Location')
        except urllib.error.HTTPError as e:
            if e.code != 201:
                print(e.code)
                print(e.read())
                print(e.headers)
                print(e.msg)
                sys.exit(1)
            print(e.headers)
            location = e.headers.get("Location")

        if not location:
            sys.stderr.write("No location received.\n")
            sys.exit(1)

        if onam:
            of = open(onam, "wb")
        else:
            of = tempfile.NamedTemporaryFile(suffix=".jpg", delete=False)
            onam = of.name
        print("Scanning to: %s" % onam)
        req = urllib.request.Request(url = location + "/NextDocument")
        data = urllib.request.urlopen(req)
        of.write(data.read())
        of.close()
        print("Scan saved to: %s" % of.name)


# Usage: python escl_scanner.py "10.1.1.1" "80" "filename.jpg"
if __name__ == "__main__":
    ip = sys.argv[1] if len(sys.argv) > 1 else "127.0.0.1"
    port = sys.argv[2] if len(sys.argv) > 2 else 80
    filename = sys.argv[3] if len(sys.argv) > 3 else None
    scanner = ESCLScanner(ip, port)
    scanner.scan(filename)
